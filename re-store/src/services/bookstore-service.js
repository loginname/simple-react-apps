export default class BookStoreService {
  data = [
    { id: 1,
      title: 'Production-Ready Microservices',
      author: 'Fowler',
      price: 50,
      coverImage: 'https://images-na.ssl-images-amazon.com/images/I/41yJ75gpV-L._SX381_BO1,204,203,200_.jpg' },
    { id: 2,
      title: 'Release It!',
      author: 'Nygard',
      price: 30,
      coverImage: 'https://images-na.ssl-images-amazon.com/images/I/41yJ75gpV-L._SX381_BO1,204,203,200_.jpg' },
  ];

  getBooks() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (Math.random() > 0.75) {
          reject(new Error('Something went wrong!!'))
        } else {
          resolve(this.data);
        }
      }, 700);
    });
  }
}


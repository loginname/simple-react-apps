const booksRequested = () => {
  return {
    type: 'FETCH_BOOKS_REQUEST',
  };
};

const booksLoaded = (newBooks) => {
  return {
    type: 'FETCH_BOOKS_SUCCESS',
    payload: newBooks,
  };
};

const booksError = (error) => {
  return {
    type: 'FETCH_BOOKS_FAILURE',
    payload: error,
  };
};

const addBookToCart = (bookId) => {
  return {
    type: 'ADD_BOOK_TO_CART',
    payload: bookId,
  }
};

const removeBookFromCart = (bookId) => {
  return {
    type: 'REMOVE_BOOK_FROM_CART',
    payload: bookId,
  };
};

const removeBooksFromCart = (bookId) => {
  return {
    type: 'REMOVE_ALL_BOOKS_FROM_CART',
    payload: bookId,
  };
};

const fetchBooks = (bookstoreService, dispatch) => () => {
  dispatch( booksRequested() );
  bookstoreService.getBooks()
    .then((data) => dispatch( booksLoaded(data) ))
    .catch((err) => dispatch( booksError(err) ));
};

export {
  fetchBooks,
  addBookToCart,
  removeBookFromCart,
  removeBooksFromCart,
};


import React, { Component } from 'react';
import { connect } from 'react-redux';

import { compose } from '../../utils';
import { fetchBooks, addBookToCart } from '../../actions';
import { withBookstoreService } from '../hoc';
import Spinner from '../spinner';
import ErrorIndicator from '../error-indicator';
import BookListItem from '../book-list-item';
import './book-list.css';


const BookList = ({ books, onAddToCart }) => {
  return (
    <ul className="book-list">
      {
        books.map((book) => {
          return (
            <li key={book.id}>
              <BookListItem
                book={book} 
                onAddToCart={() => onAddToCart(book.id)}
              />
            </li>
          );
        })
      }
    </ul>
  );
};

class BookListContainer extends Component {
  componentDidMount() {
    this.props.fetchBooks();
  }

  render() {
    const { books, loading, error, onAddToCart } = this.props;

    if (loading) {
      return <Spinner />;
    }
    if (error) {
      return <ErrorIndicator />;
    }
    return <BookList books={books} onAddToCart={onAddToCart} />;
  }
};

const mapStateToProps = ({ bookList: { books, loading, error } }) => {
  return { books, loading, error };
};

const mapDispatchToProps = (dispatch, { bookstoreService }) => {
  return {
    fetchBooks: fetchBooks(bookstoreService, dispatch),
    onAddToCart: (id) => dispatch( addBookToCart(id) ),
  };
};

export default compose(
  withBookstoreService(),
  connect(mapStateToProps, mapDispatchToProps)
)(BookListContainer);


import React from 'react';
import ReactDOM from 'react-dom';

// import UseState from './use-state';
// import UseContext from './use-context';
import UseEffect from './use-effect';


ReactDOM.render(<UseEffect />, document.getElementById('root'));

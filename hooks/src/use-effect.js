import React, { useEffect } from 'react';


const UseEffect = ({ value }) => {
  useEffect(() => {
    console.log('mount');
  }, []); // componentDidMount

  useEffect(() => {
    console.log('mount');
  }); // componentDidUpdate

  useEffect(() => {
    return () => console.log('unmount');
  }, []); // componentWillUnmount

  return <p>{ value }</p>;
};

export default UseEffect;
